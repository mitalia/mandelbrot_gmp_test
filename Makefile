all: double.x gmp.x gmp_opt.x
double.x: mandelbrot.cpp
	g++ -g -O3 mandelbrot.cpp -o double.x
gmp.x: mandelbrot.cpp
	g++ -g -O3 mandelbrot.cpp -DUSE_GMP -lgmp -lgmpxx -ogmp.x
gmp_opt.x: mandelbrot.cpp
	g++ -g -O3 mandelbrot.cpp -DUSE_GMP -DOPTIMIZED -lgmp -lgmpxx -ogmp_opt.x
clean:
	rm -f double.x gmp.x gmp_opt.x
