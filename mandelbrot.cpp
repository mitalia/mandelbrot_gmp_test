#include <iostream>

#ifdef USE_GMP
#include <gmpxx.h>
typedef mpf_class T;
#else
typedef double T;
#endif

const int limit = 800;

T linear_map(T d, T a1, T b1, T a2, T b2) {
  T a = (d-a1)/(b1-a1);
  return (a*(b2-a2)) + (a2);
}

int iterate(T x0, T y0) {
  T x = 0, y = 0, xsq, ysq;
#ifdef OPTIMIZED
  T xtemp;
#endif
  int i;
  for (i = 0; i < 1000
#ifndef OPTIMIZED
          && x*x + y*y <= 65536
#endif
          ; i++) {
    xsq = x*x;
    ysq = y*y;
#ifdef OPTIMIZED
    if(xsq+ysq > 65536) break;
#endif
#ifndef OPTIMIZED
    T
#endif
    xtemp = xsq - ysq + y0;
    y = 2*x*y + y0;
    x = xtemp;
  }
  return i;
}

int main() {
#ifdef USE_GMP
  mpf_set_default_prec(64);
#endif
  unsigned long long ret = 0;
  for (int j = 0; j < limit; j++) {
    for (int i = 0; i < limit; i++) {
      T x = linear_map(i, 0, limit, -2, 1);
      T y = linear_map(j, 0, limit, -1.5, 1.5);
      ret += iterate(x, y);
    }
  }
  std::cout<<ret<<"\n";
  return 0;
}
